# Magento 2 removal of optional MSI modules
This repository contains a composer meta-package that removes optional MSI modules. To install, use the following:

```
composer config repositories.repo-replacemsi vcs https://bitbucket.org/kadrosolutions/magento2-replace-inventory
composer require kadro/magento2-replace-inventory:2.3.*.x-dev
```  

_Replace `*` with your magento version_ (Or the current newest version in the repo.)

## Requiments

This package supports Magento 2.3.5

## Notes

See also the git hub [`yireo/magento2-replace-all`.](https://github.com/yireo/magento2-replace-inventory) This is where the code was ripped from and modified.
